#!/bin/bash

get_latest_version () {
   local builds=$(curl -s https://www.jetbrains.com/updates/updates.xml |\
                  xmllint --xpath "//product[@name='IntelliJ IDEA']/channel[@id='IC-IU-RELEASE-licensing-RELEASE']" - )
   local latestNumber=$(echo $builds | xmllint --xpath "//build/@number" - |\
                        sed 's/number=\"\([[:digit:]]*\.[[:digit:]]*\)\"/\1/g' | tr " " "\n" | sort -bnr | head -1)
   local latestVersion=$(echo $builds | xmllint --xpath "string(//build[@number=$latestNumber]/@version)" -)
   echo "ideaIC-$latestVersion"
}

archive_name_to_download="$(get_latest_version).tar.gz"

wget -q --show-progress "https://download.jetbrains.com/idea/$archive_name_to_download" && \
    tar xzfv $archive_name_to_download && \
	rm $archive_name_to_download
