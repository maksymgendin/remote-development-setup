FROM dorowu/ubuntu-desktop-lxde-vnc:focal

LABEL maintainer="maksym.gendin@gmail.com"

COPY download_latest_intellij.sh /tmp/

# for Azure:
# sed -i -e 's/mirror\:\/\/mirrors\.ubuntu.com\/mirrors\.txt/http\:\/\/azure\.archive\.ubuntu\.com\/ubuntu\//g' /etc/apt/sources.list

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y libxml2-utils wget bash nano git

RUN cd /tmp && \
    bash download_latest_intellij.sh && \
    mv idea* /opt/idea && \
    ln -s /opt/idea/bin/idea.sh /usr/local/bin/idea.sh

COPY intellij.desktop /usr/share/applications/intellij.desktop